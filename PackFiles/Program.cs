﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PackFiles {
	
    class MainClass {

        public static void Main(string[] args) {
            while (true) {
                Console.WriteLine("－－－－－－");
                Console.WriteLine("－打包程序－");
                Console.WriteLine("－请输入配置文件名（以回车结束）:");
                Console.Write("->");
                string input = Console.ReadLine();
                List<string> fileList = GetFileList(input);
                if (fileList.Count != 0) {
                    Console.WriteLine("共包含{0}个文件.", fileList.Count);
                    string name = Path.GetFileNameWithoutExtension(input) + ".pak";
                    if (PackedFile.CreatePackage(fileList, name)) {
                        Console.WriteLine("资源包{0}创建成功!", name);
                    }
                    else {
                        Console.WriteLine("资源包创建失败!");
                    }
                }
                else {
                    Console.WriteLine("未找到文件!");
                }//*/
                while (true) {
					Console.WriteLine("－按1-4键分别测试各速度模式，按5测试所有速度模式");
					Console.WriteLine("－按q键，测试正确性");
                    Console.WriteLine("－按e键，退出程序");
                    Console.Write("->");
                    char key = Console.ReadKey().KeyChar;
                    Console.WriteLine();
                    if (key == 'e') {
                        return;
                    }
                    if (key == 'q') {
                        Test.QuickTest();
                        continue;
                    }
                    if (key >= '1' && key <= '5') {
                        List<string> testFile = GetFileList("test.cfg");
                        List<string> testPackage = new List<string> { "test.pak" };
                        Test.SpeedTest(key - '1', testFile, testPackage, 1);
                        continue;
                    }
                    break;
                }
            }
        }


		/*
		 * 根据配置文件的信息，搜索所有待打包文件的列表
		 */
        private static List<string> GetFileList(string configName) {
            List<string> listCmd = new List<string>();
            StreamReader sr = null;
            try {
                sr = new StreamReader(configName);
                string temp;
                while (true) {
                    temp = sr.ReadLine();
                    if (temp != null) {
                        listCmd.Add(temp);
                    }
                    else {
                        break;
                    }
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            finally {
                if (sr != null) {
                    sr.Close();
				}
            }
            List<string> fileList = new List<string>();
            foreach (var item in listCmd) {
                if (Directory.Exists(item)) {
                    FileInfo[] files = new DirectoryInfo(item).GetFiles("*", SearchOption.AllDirectories);
                    foreach (var file in files) {
                        fileList.Add(file.FullName);
                    }
                }
                else if (File.Exists(item)) {
                    fileList.Add(item);
                }
                else {
                    Console.WriteLine("未找到{0}", item);
                }
            }
            return fileList;
        }
    }

}
