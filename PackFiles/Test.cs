﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Text;

namespace PackFiles {

    public class Test {

        private const int PackageNum = 5;
        private const int InitFileNumInFolder = 0;
        private const int InitFolderNumInFolder = 8;
        private const int RepeatTimesInSpeedTest = 1000;
        private static ArrayList TestProcess = new ArrayList{
            "文件读取       ", false, PackedFile.FileSearchMode.OnlyInFile,
            "打包读取       ", false, PackedFile.FileSearchMode.OnlyInPackage,
            "文件读取(多线程)", true, PackedFile.FileSearchMode.OnlyInFile,
            "打包读取(多线程)", true, PackedFile.FileSearchMode.OnlyInPackage,
        };

        /*
         * 测试读取的api速度，并计算结果
         */
		public static void SpeedTest(int index, List<string> testFile, List<string> testPackage, int repeatTimes) {
			long tLoad = 0, lenRead;
			long[] tRead = new long[TestProcess.Count / 3];
			int[] testIndex = new int[RepeatTimesInSpeedTest];
			Random r = new Random ();
			bool random = true;

			Console.WriteLine ("－－－－－－");
			Console.WriteLine ("－测试程序－");

			PackedFile.ReadTestFile ("/Users/apple/Downloads/xamarin.pkg", 2, true);
			Console.WriteLine ("{1}读取{0}次文件.", testIndex.Length, random? "随机" : "顺序");
			Console.WriteLine ("平均\t载入\t读取\t读取\t读取\t读取");
			Console.WriteLine ("字节\t耗时\t耗时\t时间\t速度\t模式");
            Console.WriteLine("KB\tms\tms\tms/次\tMB/S");
			do {
				for (int i = 0; i < testIndex.Length; i++) {
					testIndex[i] = random? r.Next(0, testIndex.Length) + 8000 : (i + 8000);
					//testIndex[i] = random? r.Next(0, testFile.Count) : (i % testFile.Count);
                }
				Stopwatch sw = new Stopwatch ();
				PackedFile.UnLoadAllPackage ();
				GC.Collect ();
				sw.Restart ();
				foreach (var item in testPackage) {
					if (!PackedFile.LoadPackage (item)) {
						Console.WriteLine ("加载资源包{0}失败。", item);
					}
				}
				sw.Stop ();
				tLoad = sw.ElapsedMilliseconds;

				int start, end;
				if (index >= TestProcess.Count / 3) {
					start = 0;
					end = TestProcess.Count / 3;
				} else {
					start = index;
					end = index + 1;
				}
				for (int i = start; i < end; i++) {
					GC.Collect ();
					tRead [i] = Test.DoUnitSpeedTest (
						testFile,
						testIndex,
						(bool)TestProcess [3 * i + 1],
						(PackedFile.FileSearchMode)TestProcess [3 * i + 2]);
				}

				lenRead = 0;
				for (int i = 0; i < testIndex.Length; i++) {
					lenRead += new FileInfo (testFile [testIndex [i]]).Length;
				}

				for (int i = start; i < end; i++) {
					Console.WriteLine ("{0}\t{1}\t{2}\t{3:F}\t{4:F}\t{5}",
						lenRead / 1024 / testIndex.Length,
						(PackedFile.FileSearchMode)TestProcess [3 * i + 2] == PackedFile.FileSearchMode.OnlyInFile ? 0 : tLoad,
						tRead [i],
						(float)tRead [i] / testIndex.Length,
						(float)lenRead * 1000 / tRead [i] / 1024 / 1024,
						(string)TestProcess [3 * i]);
				}
				repeatTimes--;
			} while(repeatTimes > 0);
		}

        /*
         * 根据指定模式读取指定的文件列表
         */
        public static long DoUnitSpeedTest(List<string> fileList, int[] index, bool MultiThread, PackedFile.FileSearchMode mode) {
            Stopwatch sw = new Stopwatch();
            PackedFile.FileSearchMode temp = PackedFile.fileSearchMode;
            PackedFile.SetPackedMode(mode);

            if (MultiThread) {
                Task[] tasks = new Task[index.Length];
                sw.Restart();
                for (int i = 0; i < index.Length; i++) {
                    tasks[i] = new Task(TaskUnitSpeedTest, fileList[index[i]]);
                    tasks[i].Start();
                }
                Task.WaitAll(tasks);
            }
            else {
                sw.Restart();
                for (int i = 0; i < index.Length; i++) {
                    TaskUnitSpeedTest(fileList[index[i]]);
                }
            }
            sw.Stop();

            PackedFile.SetPackedMode(temp);
            return sw.ElapsedMilliseconds;
        }

        /*
         * 速度测试任务。打开文件，全部读取后关闭
         */
        private static Action<object> TaskUnitSpeedTest = (object obj) =>
        {
            PackedFile file = PackedFile.OpenFile((string)obj);
            if (file != null) {
                byte[] content = file.ReadToEnd();
                file.Close();
            }
        };

        /*
         * 快速测试。自建测试文件，用于快速验证API正确性。
         */
        public static void QuickTest() {
            List<string> directoryList = new List<string>(PackageNum);
            for (int i = 0; i < PackageNum; i++) {
                directoryList.Add(i.ToString());
            }
            List<string> fileList = new List<string>();
            foreach (var directory in directoryList) {
                AddFile(InitFileNumInFolder, InitFolderNumInFolder, directory, fileList);
            }
            List<string> packageList = new List<string>(directoryList.Count);
            for (int i = 0; i < directoryList.Count; i++) {
                packageList.Add(GetPackageName(directoryList[i]));
            }

            while (true) {
                Console.WriteLine("－－－－－－");
                Console.WriteLine("－测试程序－");
                Console.WriteLine("功能列表，请输入对应数字:");
                Console.WriteLine("1.生成测试文件夹、文件、资源包");
                Console.WriteLine("2.测试读取正确性");
                Console.WriteLine("3.删除资源包、删除测试文件夹及文件");
                Console.WriteLine("按e键退出测试程序");
                Console.WriteLine("－－－－－－");
                Console.Write("->");
                while (true) {
                    char c = Console.ReadKey().KeyChar;
                    Console.WriteLine();
                    switch (c) {
                    case '1':
                        StreamWriter sw;
                        foreach (var item in fileList) {
                            if (!File.Exists(item)) {
                                string directory = Path.GetDirectoryName(item);
                                if (directory != "") {
                                    Directory.CreateDirectory(directory);
                                }
                                sw = new StreamWriter(item);
                                sw.Write(item);
                                sw.Close();
                            }
                        }
                        Console.WriteLine("测试文件创建完成. 文件数:{0}.", fileList.Count);
                        foreach (var item in directoryList) {
                            DirectoryInfo directory =  new DirectoryInfo(item);
                            if (directory.Exists) {
                                FileInfo[] files = directory.GetFiles("*", SearchOption.AllDirectories);
                                List<string> subfileList = new List<string>();
                                foreach (var file in files) {
                                    subfileList.Add(file.FullName);
                                }
                                PackedFile.CreatePackage(subfileList, GetPackageName(item));
                            }
                        }
                        Console.WriteLine("测试资源包创建完成. 资源数:{0}.", directoryList.Count);
                        break;
                    case '2':
                        Console.WriteLine("正确性测试开始.");
                        Test.TestCorrect(fileList, packageList, false);
                        Console.WriteLine("正确性测试完成.");
                        Console.WriteLine("正确性测试开始.（多线程）");
                        Test.TestCorrect(fileList, packageList, true);
                        Console.WriteLine("正确性测试完成.（多线程）");
                        break;
                    case '3':
                        PackedFile.UnLoadAllPackage();
                        foreach (var item in packageList) {
                            if (File.Exists(item)) {
                                File.Delete(item);
                            }
                        }
                        Console.WriteLine("资源包删除完成.");
                        foreach (var item in directoryList) {
                            if (Directory.Exists(item)) {
                                Directory.Delete(item, true);
                            }
                        }
                        Console.WriteLine("测试文件删除完成.");
                        break;
                    case 'e':
                        return;
                    default:
                        continue;
                    }
                    break;
                }
            }
        }

        /*
         * 测试读取的正确性。测试文件的内容与其地址相同，对比即可判断是否读对。
         */
        public static void TestCorrect(List<string> fileList, List<string> packageList, bool MultiThread) {
            foreach (var item in packageList) {
                if (!PackedFile.LoadPackage(item)) {
                    Console.WriteLine("加载资源包{0}失败。", item);
                }
            }
            if (MultiThread) {
                Task[] tasks = new Task[fileList.Count];
                for (int i = 0; i < fileList.Count; i++) {
                    tasks[i] = new Task(TaskTestCorrect, fileList[i]);
                    tasks[i].Start();
                }
                Task.WaitAll(tasks);
            }
            else {
                foreach (var item in fileList) {
                    TaskTestCorrect(item);
                }
            }
        }

        /*
         * 正确性测试任务。打开文件，全部读取后判读是否等于路径，然后关闭
         */
        private static Action<object> TaskTestCorrect = (object obj) =>
        {
            string item = (string)obj;
            PackedFile file = PackedFile.OpenFile(item);
            if (file != null) {
                string content = System.Text.Encoding.UTF8.GetString(file.ReadToEnd());
                Console.WriteLine("文件 {0} 读取结果: {1}.", item, item == content);
                //Console.WriteLine("内容:{0}", content);
                file.Close();
            }
            else {
                Console.WriteLine("文件 {0} 打开失败.", item);
            }
        };

        /*
         * 递归添加需要创建的文件路径
         */
        private static void AddFile(int fileNum, int folderNum, string directory, List<string> list) {
            while (folderNum > 0) {
                AddFile(fileNum + 1, folderNum - 1, Path.Combine(directory, folderNum.ToString()), list);
                folderNum--;
            }
            while (fileNum > 0) {
                list.Add(Path.Combine(directory, fileNum + ".txt"));
                fileNum--;
            }
        }

        /*
         * 资源包的命名
         */
        private static string GetPackageName(string directory) {
            return "Temp_" + Path.GetFileNameWithoutExtension(directory) + ".pak";
        }

    }
}
