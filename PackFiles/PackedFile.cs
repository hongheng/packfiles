﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;
using System.Diagnostics;

namespace PackFiles
{
    public abstract class PackedFile {

        /*
         * 文件的存储信息
         */
		private class FileInf{
            public int offset { get; private set; }
            public int size { get; private set; }
            public string name { get; private set; }

            public FileInf(int offset, int size, string name) {
				this.offset = offset;
				this.size = size;
                this.name = name;
			}
		}

		/*
		 * 文件的索引信息
		 */
		private class PackedFileInf{
            public int package { get; private set; }
			public int offset { get; private set; }
			public int size { get; private set; }

			public PackedFileInf(int package, int offset, int size){
                this.package = package;
				this.offset = offset;
				this.size = size;
			}
		}

		/*
		 * 资源包文件的信息
		 */
		private class PackageInf{
			public FileStream fileStream { get; private set; }
			public Object SyncRoot { get; private set; }

			public PackageInf(FileStream fileStream){
				this.fileStream = fileStream;
				SyncRoot = new Object();
			}
		}

		/*
		 * 文件搜索模式，包括资源包搜索模式、文件系统搜索模式，以及兼容模式（资源包优先搜索）
		 */
		public enum FileSearchMode {
			OnlyInPackage,
			OnlyInFile,
			BothInPackageAndFile,
		}

        public static FileSearchMode fileSearchMode { get; private set; }
        private static List<PackageInf> packageList; //资源包列表
        private static List<PackedFileInf> fileInfList; //文件偏移信息列表
        private static Hashtable fileIndexTable; //文件偏移信息的索引哈希表：｛文件名，其索引信息在数组中的下标｝
        private static string rootPath;
        private const bool READFILE_WITHOUT_BUFFER = false;
        private const int MIN_LEN_TO_READ_FILE = 512;

        static PackedFile() {
            fileSearchMode = FileSearchMode.OnlyInPackage;//默认采用打包方式读取文件
            packageList = new List<PackageInf>(); 
            fileInfList = new List<PackedFileInf>(); 
            fileIndexTable = new Hashtable();
            rootPath = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar;
        }

		/*
		 * 根据文件列表创建资源包文件
		 */
        public static bool CreatePackage(List<string> fileList, string packageNameWithPath) {
            if (fileList == null || string.IsNullOrEmpty(packageNameWithPath)) {
                return false;
            }
            List<FileInf> infList = new List<FileInf>();
            Int32 offset = 0;
            FileStream fsDst = null;
            BinaryWriter bw = null;
            bool result = false;
            try {
                //允许重写
                fsDst = new FileStream(packageNameWithPath, FileMode.Create, FileAccess.Write);
                bw = new BinaryWriter(fsDst);

                // 为信息地址预留位置
                bw.Write((Int32)0);
                offset += 4;
                
                //复制文件
                FileStream fsSrc = null;
                foreach (var item in fileList) {
                    fsSrc = new FileStream(item, FileMode.Open, FileAccess.Read);
                    fsSrc.CopyTo(fsDst);

                    //记录索引信息
                    infList.Add(new FileInf(offset, (int)fsSrc.Length, NormalizePath(item)));
                    offset += (int)fsSrc.Length;

                    fsSrc.Close();
                }

                // 写入索引信息
                bw.Write(infList.Count); // 写入文件数量
                foreach (var item in infList) {
                    bw.Write(item.offset);
                    bw.Write(item.size);
                    char[] temp = item.name.ToCharArray();
                    bw.Write(temp.Length);
                    bw.Write(temp);
                }

                // 回到文件头，写入信息地址
                bw.Seek(0, SeekOrigin.Begin);
                bw.Write(offset);

                result = true;
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            finally {
                if (bw != null) {
                    bw.Close();
                }
                if (fsDst != null) {
                    fsDst.Close();
                }
            }
            return result;
        }

		/*
		 * 加载资源包文件
		 */
        public static bool LoadPackage(string packageNameWithPath) {
            if (fileSearchMode == FileSearchMode.OnlyInFile) {
                return true;
            }
            FileStream fs = null;
            BinaryReader br = null;
            bool result = false;
            try {
                fs = new FileStream(packageNameWithPath, FileMode.Open, FileAccess.Read);
                br = new BinaryReader(fs);

                int offset = br.ReadInt32();
                if (offset < 0 || offset > fs.Length) {
                    return false;
                }

                fs.Seek(offset, SeekOrigin.Begin);
                int num = br.ReadInt32();
                FileInf[] fileInfs = new FileInf[num];
                for (int i = 0; i < num; i++) {
                    int fileOffset = br.ReadInt32();
                    int fileSize = br.ReadInt32();
                    int fileNameLen = br.ReadInt32();
                    char[] name = br.ReadChars(fileNameLen);
                    string fileName = new string(name);
                    fileInfs[i] = new FileInf(fileOffset, fileSize, fileName);
                }
                if (READFILE_WITHOUT_BUFFER) {
					br.Close();
                    fs.Close();
                    fs = GetDirectFileStream(packageNameWithPath);
                }
                PackageInf pi = new PackageInf(fs);
                int packageIndex;
                lock (((ICollection)packageList).SyncRoot) {
                    packageIndex = packageList.Count;
                    packageList.Add(pi);
                }

                int fileStartIndex;
                lock (((ICollection)fileInfList).SyncRoot) {
                    fileStartIndex = fileInfList.Count;
                    for (int i = 0; i < fileInfs.Length; i++) {
                        fileInfList.Add(new PackedFileInf(packageIndex, fileInfs[i].offset, fileInfs[i].size));
                    }
                }

                lock (((ICollection)fileIndexTable).SyncRoot) {
                    for (int i = 0; i < fileInfs.Length; i++) {
                        //fileIndexTable.Add(fileInfs[i].name, fileStartIndex + i);
                        fileIndexTable[fileInfs[i].name] = fileStartIndex + i;
                    }
                }

                result = true;
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
            return result;
        }

		/*
		 * 打开文件
		 */
		public static PackedFile OpenFile(string fileNameWithPath){
			if (fileNameWithPath == null || fileNameWithPath == "") {
				return null;
			}
			fileNameWithPath = NormalizePath(fileNameWithPath);
			PackedFile result = null;
			if (fileSearchMode != FileSearchMode.OnlyInFile) {
                //先从哈希表中获取文件信息的索引
                object o;
                lock (((ICollection)fileIndexTable).SyncRoot) {
                    o = fileIndexTable[fileNameWithPath];
                }
                if (o != null) {
                    PackedFileInf pfi = null;
                    PackageInf pi = null;
                    int index = (int)o;
                    //再从信息列表中取出偏移信息
                    lock (((ICollection)fileInfList).SyncRoot) {
                        if ((index >= 0) && (index < fileInfList.Count)) {
                            pfi = fileInfList[index];
                        }
                    }
                    if (pfi != null) {
                        lock (((ICollection)packageList).SyncRoot) {
                            pi = packageList[pfi.package];
                        }
                        result = new PackedFileInPakage(pfi, pi);
                    }
                }
			} 
			if (fileSearchMode != FileSearchMode.OnlyInPackage && result == null) {
                try {
                    FileStream fs;
                    if (READFILE_WITHOUT_BUFFER) {
                        fs = GetDirectFileStream(fileNameWithPath);
                    }
                    else {
                        fs = new FileStream(fileNameWithPath, FileMode.Open, FileAccess.Read);
                    }
					result = new PackedFileInFile (fs);
				} catch (Exception ex) {
					Console.WriteLine (ex.Message);
				}
			}
			return result;
		}

		/*
		 * 关闭文件
		 */
		public static void CloseFile(PackedFile file){
			if (file != null) {
				file.Close ();
			}
		}

		/*
		 * 卸载所有资源包，便于测试
		 */
        public static void UnLoadAllPackage() {
            lock (((ICollection)fileIndexTable).SyncRoot) {
                fileIndexTable.Clear();
            }
            lock (((ICollection)fileInfList).SyncRoot) {
                fileInfList.Clear();
            }
			lock (((ICollection)packageList).SyncRoot) {
				foreach (var item in packageList) {
					item.fileStream.Close ();
				}
				packageList.Clear ();
			}
		}

		/*
		 * 设置打包模式，便于测试
		 */
		public static void SetPackedMode(FileSearchMode mode){
			fileSearchMode = mode;
		}

		/*
		 * 统一路径格式
		 */
		private static string NormalizePath(string path){
			if (path.StartsWith (rootPath)) {
				path = path.Remove (0, rootPath.Length); //统一处理为相对路径
			}
			return path.Replace('\\','/'); //统一处理为'/'分隔符
		}

		/*
		 * 读取整个文件
		 */
		public abstract byte[] ReadToEnd();

		/*
		 * 从指定位置读取指定长度字节到缓存的指定位置中
		 * 输入参数：	index，需要读取的内容在文件中的索引位置
		 * 			len，  需要读取的内容的字节长度
		 * 			buffer，保存内容的目标缓存
		 * 			offset，缓存的起始保存位置
		 */
		public abstract int Read(int index, int len, byte[] buffer, int offset);

		/*
		 * 关闭文件
		 */
		public abstract void Close();

		/*
		 * 获取文件大小
		 */
        public abstract long GetSize();

		/*
		 * 在文件系统中进行文件操作的子类
		 */
        private class PackedFileInFile : PackedFile {
		    private FileStream fileStream;

		    public PackedFileInFile(FileStream fileStream){
			    this.fileStream = fileStream;
		    }

            public override byte[] ReadToEnd() {
			    byte[] bytes = null;
                if (fileStream != null) {
                    bytes = new byte[fileStream.Length];
                    if (!READFILE_WITHOUT_BUFFER) {
                        fileStream.Seek(0, SeekOrigin.Begin);
                        fileStream.Read(bytes, 0, (int)fileStream.Length);
                    }
                    else {
                        int len = (int)fileStream.Length;
                        int t = len % MIN_LEN_TO_READ_FILE;
                        if (t != 0) {
                            len += MIN_LEN_TO_READ_FILE - t;
                        }
                        byte[] bytesTemp = new byte[len];
                        fileStream.Seek(0, SeekOrigin.Begin);
                        fileStream.Read(bytesTemp, 0, len);
                        Array.Copy(bytesTemp, 0, bytes, 0, bytes.Length);
                    }
                } else {
				    // the filestream already close.
			    }
			    return bytes;
		    }

            public override int Read(int index, int length, byte[] buffer, int offset) {
                if (fileStream != null) {
                    if (!READFILE_WITHOUT_BUFFER) {
                        fileStream.Seek(index, SeekOrigin.Begin);
                        return fileStream.Read(buffer, offset, length);
                    }
                    else {
                        int start = index;
                        int len = length;
                        int rStart = start % MIN_LEN_TO_READ_FILE;
                        if (rStart != 0) {
                            start -= rStart;
                            len += rStart;
                        }
                        int t = len % MIN_LEN_TO_READ_FILE;
                        if (t != 0) {
                            len += MIN_LEN_TO_READ_FILE - t;
                        }
                        byte[] bytesTemp = new byte[len];
                        fileStream.Seek(start, SeekOrigin.Begin);
                        int result = fileStream.Read(bytesTemp, 0, len);
                        Array.Copy(bytesTemp, rStart, buffer, offset, result);
                        return result;
                    }
                }
                else {
                    return 0;
                }
            }

            public override void Close() {
			    if (fileStream != null) {
				    fileStream.Close ();
				    fileStream = null;
			    }
		    }

            public override long GetSize() {
                if (fileStream != null) {
                    return fileStream.Length;
                }
                else {
                    return 0;
                }
            }
        }

		/*
		 * 在资源包中进行文件操作的子类
		 */
        private class PackedFileInPakage : PackedFile {
		    private PackedFileInf fileInf;
		    private PackageInf resourceInf;

            public PackedFileInPakage(PackedFileInf fileInf, PackageInf resourceInf) {
			    this.fileInf = fileInf;
			    this.resourceInf = resourceInf;
		    }

		    /*
		     * 读取文件
		     */
		    public override byte[] ReadToEnd(){
			    byte[] bytes = null;
                if ((fileInf != null) && (resourceInf != null)) {
                    bytes = new byte[fileInf.size];
                    if (!READFILE_WITHOUT_BUFFER) {
                        lock (resourceInf.SyncRoot) {
                            resourceInf.fileStream.Seek(fileInf.offset, SeekOrigin.Begin);
                            resourceInf.fileStream.Read(bytes, 0, fileInf.size);
                        }
                    }
                    else {
                        int start = fileInf.offset;
                        int len = fileInf.size;
                        int rStart = start % MIN_LEN_TO_READ_FILE;
                        if (rStart != 0) {
                            start -= rStart;
                            len += rStart;
                        }
                        int t = len % MIN_LEN_TO_READ_FILE;
                        if (t != 0) {
                            len += MIN_LEN_TO_READ_FILE - t;
                        }
                        byte[] bytesTemp = new byte[len];
                        lock (resourceInf.SyncRoot) {
                            resourceInf.fileStream.Seek(start, SeekOrigin.Begin);
                            resourceInf.fileStream.Read(bytesTemp, 0, len);
                        }
                        Array.Copy(bytesTemp, rStart, bytes, 0, bytes.Length);
                    }
                } 
			    return bytes;
		    }

            public override int Read(int index, int length, byte[] buffer, int offset) {
                if ((fileInf != null) && (resourceInf != null)) {
                    int result;
                    if (!READFILE_WITHOUT_BUFFER) {
                        lock (resourceInf.SyncRoot) {
                            resourceInf.fileStream.Seek(index, SeekOrigin.Begin);
                            result = resourceInf.fileStream.Read(buffer, offset, length);
                        }
                        return result;
                    }
                    else {
                        int start = index;
                        int len = length;
                        int rStart = start % MIN_LEN_TO_READ_FILE;
                        if (rStart != 0) {
                            start -= rStart;
                            len += rStart;
                        }
                        int t = len % MIN_LEN_TO_READ_FILE;
                        if (t != 0) {
                            len += MIN_LEN_TO_READ_FILE - t;
                        }
                        byte[] bytesTemp = new byte[len];
                        lock (resourceInf.SyncRoot) {
                            resourceInf.fileStream.Seek(start, SeekOrigin.Begin);
                            result = resourceInf.fileStream.Read(bytesTemp, 0, len);
                        }
                        Array.Copy(bytesTemp, rStart, buffer, offset, result);
                        return result;
                    }
                }
                else {
                    return 0;
                }
            }

		    /*
		     * 关闭文件
		     */
            public override void Close() {
		    }

            public override long GetSize() {
                if ((fileInf != null)) {
                    return fileInf.size;
                }
                else {
                    return 0;
                }
            }
        }

		/*
		 * 导入win32 api: CreateFile
		 */
        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern SafeFileHandle CreateFile(string lpFileName, uint dwDesiredAccess,
          uint dwShareMode, IntPtr lpSecurityAttributes, uint dwCreationDisposition,
          uint dwFlagsAndAttributes, IntPtr hTemplateFile);

        private const uint GENERIC_READ = 0x80000000;
        private const uint FILE_SHARE_READ = 0x00000001;
        private const uint NO_BUFFERING = 0x20000000;
        private const uint FILE_FLAG_OVERLAPPED = 0x40000000;
        private const uint OPEN_EXISTING = 3;

		/*
		 * 获取不带文件缓存的文件流，仅windows平台下有效
		 */
        private static FileStream GetDirectFileStream(string path) {
            SafeFileHandle handleValue = CreateFile(path, GENERIC_READ, FILE_SHARE_READ, IntPtr.Zero, OPEN_EXISTING, NO_BUFFERING, IntPtr.Zero);// | FILE_FLAG_OVERLAPPED
            return new FileStream(handleValue, FileAccess.Read, 8);//, true
        }

		/*
		 * 读取测试文件，并显示读取速度，用于测试文件读取接口
		 */
		public static void ReadTestFile(string fileName, int times, bool log) {
            FileStream s;
			long len;
            if (READFILE_WITHOUT_BUFFER) {
				s = GetDirectFileStream(fileName);
				len = 50 * 1024 * 1024; // s.Length; //512; //  
            }
            else {
				s = new FileStream(fileName, FileMode.Open, FileAccess.Read);
				len = s.Length; //512; //  
            }
            byte[] b = new byte[len];
            Stopwatch sw = new Stopwatch();
            for (int i = 0; i < times; i++) {
                sw.Restart();
                s.Seek(0, SeekOrigin.Begin);
                int x = s.Read(b, 0, b.Length);
                sw.Stop();
                long t = sw.ElapsedMilliseconds;
                if (t == 0) t = 1;
				if (log) {
					Console.WriteLine ("ReadTestFile: {0}ms. {1}MB/s. {2:0,000,000}B", t, x / t / 1000, x);
				}
            }
            s.Close();
        }
	}
}